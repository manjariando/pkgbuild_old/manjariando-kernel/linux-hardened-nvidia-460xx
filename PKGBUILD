# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Contributor: Joan Bruguera <joanbrugueram@gmail.com> (kernel-5.12.patch)

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

_linuxprefix=linux-hardened
_extramodules=extramodules-5.14-hardened
# don't edit here
pkgver=460.91.03_5.14.21.hardened1_1

_nver=460
# edit here for new version
_sver=91.03
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-460xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
makedepends=("$_linuxprefix" "$_linuxprefix-headers" "nvidia-${_nver}xx-utils=${_pkgver}")
groups=("$_linuxprefix-extramodules")
provides=("$_pkgname=$pkgver")
conflicts=("$_linuxprefix-nvidia" "$_linuxprefix-nvidia-304xx" "$_linuxprefix-nvidia-340xx"
            "$_linuxprefix-nvidia-390xx" "$_linuxprefix-nvidia-418xx" "$_linuxprefix-nvidia-430xx"
           "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx"
           "$_linuxprefix-nvidia-455xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="https://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        'kernel-5.14.patch')
sha256sums=('448156cfcef182ed6997c2754c472fd681bf7139b821d2adce1d847220c6c933'
            'd778d2faf705fd4db324cad4ee0cc42f10f5fe6ba5802806b6edd9d667934672')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here
    # Fix compile problem with 5.14
    # https://gist.github.com/joanbm/144a965c36fc1dc0d1f1b9be3438a368
    msg2 "PATCH: kernel-5.14"
    patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.14.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}" "nvidia-${_nver}xx-utils=${_pkgver}")

    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-modeset.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-modeset.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-drm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-drm.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia-uvm.ko" \
         "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
